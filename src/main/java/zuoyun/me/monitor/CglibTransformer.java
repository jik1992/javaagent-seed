package zuoyun.me.monitor;


import net.sf.cglib.proxy.CallbackFilter;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import org.apache.commons.lang3.StringUtils;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.reflect.Method;
import java.security.ProtectionDomain;
import java.text.NumberFormat;

/**
 * Created by ZuoYun on 11/21/15. Time: 1:58 AM Information:
 *
 * 动态代理
 */

public class CglibTransformer implements ClassFileTransformer {


  @Override
  public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                          ProtectionDomain protectionDomain, byte[] classfileBuffer)
      throws IllegalClassFormatException {
    System.out.println(StringUtils.center("cglib load success!", 22, "="));

    className = className.replace("/", ".");
    System.out.println(StringUtils.center(className, 22));

    //aop->cglib->asm
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(NumberFormat.class);

    //proxy 这个对象，生成一个包装类。
    enhancer.setCallback(new MethodInterceptor() {
      @Override
      public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        return null;
      }
    });

    enhancer.setCallbackFilter(new CallbackFilter() {
      @Override
      public int accept(Method method) {
        return 0;
      }
    });

    return new byte[0];
  }
}
