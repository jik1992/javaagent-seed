package zuoyun.me.monitor;


import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

import org.apache.commons.lang3.StringUtils;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

/**
 * Created by ZuoYun on 11/21/15. Time: 1:58 AM Information:
 *
 * 字节码增强
 */

public class JavassistTransformer implements ClassFileTransformer {


  @Override
  public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                          ProtectionDomain protectionDomain, byte[] classfileBuffer)
      throws IllegalClassFormatException {
    System.out.println(StringUtils.center("javassist load success!", 22, "="));

    className = className.replace("/", ".");
    System.out.println(StringUtils.center(className, 22));

    //javaassit
    CtClass ctClass = null;
    try {
      ctClass = ClassPool.getDefault().get(className);
      for (CtMethod ctMethod : ctClass.getMethods()) {
//        System.out.println(ctMethod.getLongName());
      }
    } catch (NotFoundException e) {
      e.printStackTrace();
    }
    return new byte[0];
  }
}
