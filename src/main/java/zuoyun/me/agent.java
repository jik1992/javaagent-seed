package zuoyun.me;


import org.apache.commons.lang3.StringUtils;

import java.lang.instrument.Instrumentation;

import zuoyun.me.monitor.CglibTransformer;
import zuoyun.me.monitor.JavassistTransformer;

/**
 * Created by ZuoYun on 11/21/15. Time: 12:05 AM Information:
 */

public class agent {

  private static volatile Instrumentation globalInstr;

  public static void premain(String agentArgs, Instrumentation inst) {
    System.out.println(StringUtils.center("agent load success!", 22, "="));
    inst.addTransformer(new JavassistTransformer());
    inst.addTransformer(new CglibTransformer());
    globalInstr = inst;
  }

  public static void main(String[] args) {
    System.out.println(getObjectSize(1));
    System.out.println(getObjectSize(1L));
    System.out.println(getObjectSize(1.1));
    System.out.println(getObjectSize(1.1D));
  }

  public static long getObjectSize(Object obj) {
    if (globalInstr == null) {
      throw new IllegalStateException("Agent not initted");
    }
    return globalInstr.getObjectSize(obj);
  }

}
